# Curso profesional de Git y GitHub
## Estados
1. Directorio de trabajo (working directory)
2. Área de preparación (staging area)
3. Directorio de Git (Git directory)
## Comandos
#### `git init`
Inicia un repositorio en la carpeta actual
#### `rm -rf .git`
Elimina el repositorio git
#### `git status`
Muestra el estado del repositorio
#### `git add <archivo>`
Agrega archivo al staging area
#### `git add .`
Agrega todos los archivos al staging area
#### `git rm --cached <archivo>`
Elimina el archivo del staging area
#### `git commit -m "Mensaje"`
Confirmar los cambios al repositorio
#### `git commit --amend`
Agregar los nuevos cambios al commit anterior
#### `git log`
Ver historial de commits
#### `git tag <tag>`
Crear una etiqueta con el commit actual
#### `git tag -a <tag> -m "Mensaje"`
Crear una etiqueta anotada con el commit actual
#### `git tag <tag> <commit hash>`
Crear una etiqueta con el commit indicado
#### `git tag`
Muestra las etiquetas
#### `git tag -l`
Muestra las etiquetas ordenadas
#### `git tag -d <tag>`
Elimina la etiqueta
#### `git diff <commit hash>`
Compara el commit actual con el indicado
#### `git diff <commit hash> <commit hash>`
Compara los commit indicados
#### `git reset --soft <commit hash>`
Se vuelve al commit indicado. Los archivos se mantienen y se agregan al staging area.
#### `git reset --mixed <commit hash>`
Se vuelve al commit indicado. Los archivos se mantienen y quedan en el working directory.
#### `git reset --hard <commit hash>`
Se vuelve al commit indicado. Los archivos vuelven a la versión del commit.
#### `git branch`
Listar ramas
#### `git branch <nombre rama>`
Crear nueva rama a partir de la rama actual
#### `git checkout -b <nombre rama>`
Crear nueva rama a partir de la rama actual y moverse a la misma
#### `git branch -d <nombre rama>`
Eliminar rama
#### `git branch -m <nombre rama> <nuevo nombre>`
Renombrar rama
#### `git checkout <rama>`
Moverse a la rama indicada
#### `git checkout <commit>`
Moverse al commit indicado
#### `git checkout -b <commit>`
Crea una rama a partir de un commit
#### `git merge <rama>`
Mezcla la rama indicada con la rama actual
#### `git stash`
Guarda los cambios actuales del repositorio. Esto sirve en caso de no querer confirmar los cambios, trabajar en otra rama, volver y restaurlos.
Funciona únicamente con archivos con seguimiento. En caso de ser archivos nuevos, es necesarios agregarlos al staging area.
#### `git stash list`
Lista los stash
#### `git stash drop stash@{<numero>}`
Elimina el stash indicado
#### `git stash apply stash@{<numero>}`
Reestablece el stash indicado
#### `git cherry-pick <commit>`
Extrae el commit indicado a la rama actual
#### `git clone <url>`
Clona el repositorio remoto
#### `git remote add origin <url>`
Añade un repositorio remoto al local
#### `git remote -v`
Lista los repositorios remotos
#### `git remote remove origin`
Elimina el enlace al repositorio remoto 'origin'
#### `git fetch origin <rama>`
Actualiza la rama indicada del repositorio remoto en el local
#### `git pull origin <rama>`
Realiza un fetch y un merge
#### `git push origin <rama>`
Envía los cambios al repositorio remoto
#### `git push origin --tags`
Envía las ramas al repositorio remoto